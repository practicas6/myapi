import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import Login from './components/Login'
import list from './components/ListApi'

Vue.config.productionTip = false
Vue.use(Router)

var router=new Router({
  routes:[
    {
      path:'/',
      component:Login
    },
      {
      path:'/Login',
          component:Login

      },{
      path:'/List',
          component:list
      }


  ]
});

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
